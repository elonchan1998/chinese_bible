import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter

from preprocessing import Preprocess
from attention import AttnLSTM

import pickle



if __name__ == '__main__':
    
    # Hyperparameters
    file_path = "corpus/chinese_bible.txt"
    seq_length = 50
    batch_size = 32
    
    # Models'
    lr = 0.0001
    epochs = 30
    embed_dim = 512
    hidden_dim = 512
    num_layers = 2
    dropout = 0.1
    
    # Load preprocssed data and helpers
    data = Preprocess(file_path, seq_length, batch_size, valid_ratio=0.2)
    
    # Define models
    model = AttnLSTM(len(data.word_list), embed_dim, hidden_dim, num_layers, dropout, seq_length)
    model.cuda()
    
    print(model)
    
    optimizer = Adam(model.parameters(), lr=lr)
    
    writer = SummaryWriter('runs/attn-1')
    
    # Start training
    for epoch in range(epochs):
        
        hidden = torch.zeros(1, 1, hidden_dim)
        print('hidden', hidden)
        hidden.cuda()
        
        for batch_i, (sentence, next_word) in enumerate(zip(data.train_X, data.train_y)):
            
            model.zero_grad()
            output, hidden, attn_weights = model(sentence, hidden)
            loss = F.cross_entropy(output, next_word)
            loss.backward()
            optimizer.step()
            
            if batch_i%100 == 0:
                # log the running loss
                writer.add_scalar('training loss', loss.item(), epoch * len(data.X) + batch_i)
                #print("Train epoch: {} ({:2.0f}%)\tLoss: {:.6f}".format(epoch, 100.* batch_i/len(data.X), loss.item()))
                
        model.eval()
        torch.save(model, 'models/char_level_attn.bin')
        print('Model saved')

    
    