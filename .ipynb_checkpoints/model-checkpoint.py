import torch.nn as nn
import torch.nn.functional as F

class LSTM(nn.Module):
    
    def __init__(self, input_dim, embed_dim, hidden_dim, num_layers, dropout):
        super().__init__() #inherent the nn.Module class
        
        self.embed = nn.Embedding(input_dim, embed_dim)
        self.lstm = nn.LSTM(embed_dim, hidden_dim, num_layers=num_layers, dropout=dropout)
        self.fc = nn.Linear(hidden_dim, input_dim) #Since input space equals output space
        
    def forward(self, sentence):
        
        embed = self.embed(sentence.t())
        lstm_out, (h_n, c_n) = self.lstm(embed)
        output = self.fc(lstm_out[-1])
        
        return output
    
class GRU(nn.Module):
    
    def __init__(self, input_dim, embed_dim, hidden_dim, num_layers, dropout):
        super().__init__() #inherent the nn.Module class
        
        self.embed = nn.Embedding(input_dim, embed_dim)
        self.gru = nn.GRU(embed_dim, hidden_dim, num_layers=num_layers, dropout=dropout)
        self.fc = nn.Linear(hidden_dim, input_dim) #Since input space equals output space
        
    def forward(self, sentence):
        
        embed = self.embed(sentence.t())
        gru_out, h_n = self.gru(embed)
        output = self.fc(gru_out[-1])
        
        return output