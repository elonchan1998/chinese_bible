import pickle

import torch
import torch.nn as nn
import torch.nn.functional as F

import numpy as np



if __name__ == '__main__':
    
    with open('models/data.bin', 'rb') as f:
        data = pickle.load(f)
        
    model = torch.load('models/lstm_model.bin')
    
    n, n_sent = 0, 2

    start = input('First few words: ') # 神說，要有光
    input_sent = [data.word2id[char] for char in start]

    end_chars = ["。", "？", "！", "?", "!"]

    print(start, end='')
    while n < n_sent:
        
        model_input = np.array(input_sent)
        model_input = model_input.reshape(1, -1)
        model_input = torch.cuda.LongTensor(model_input)
        
        predict = model(model_input)
        prob = F.softmax(predict, dim=1).data[0].cpu().detach().numpy()
        next_word = np.random.choice(data.word_list, p=prob)
        print(next_word, end='')

        next_word_idx = data.word2id[next_word]
        input_sent.append(next_word_idx)
        input_sent[1:]

        if next_word in end_chars:
            n += 1
    
    print('\n')
