import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import Adam
#from torch.utils.tensorboard import SummaryWriter

from preprocessing import Preprocess
from model import LSTM, GRU

import pickle



if __name__ == '__main__':
    
    # Hyperparameters
    file_path = "corpus/chinese_bible.txt"
    seq_length = 50
    batch_size = 32
    
    # Models'
    lr = 0.0001
    epochs = 10
    embed_dim = 1024
    hidden_dim = 1024
    num_layers = 2
    dropout = 0.2
    
    data = Preprocess(file_path, seq_length, batch_size)
    
    with open('models/data_cpu.bin', 'wb') as f:
        pickle.dump(data, f)

    print(brrr)
    
    #model = LSTM(len(data.word_list), embed_dim, hidden_dim, num_layers, dropout)
    model = GRU(len(data.word_list), embed_dim, hidden_dim, num_layers, dropout)
    model.cuda()
    
    print(model)
    
    #writer = SummaryWriter('runs/gru-1')
    #writer.add_graph(model, data.X[0])

    optimizer = Adam(model.parameters(), lr=lr)

    print('Training Started')
    for epoch in range(epochs):
        model.train() #signal the model to trian
        for batch_i, (sentence, next_word) in enumerate(zip(data.X, data.y)):
            model.zero_grad()
            output = model(sentence)
            loss = F.cross_entropy(output, next_word)
            loss.backward()
            optimizer.step()

            if batch_i%100 == 0:
                # log the running loss
                #writer.add_scalar('training loss', loss.item(), epoch * len(data.X) + batch_i)
                print("Train epoch: {} ({:2.0f}%)\tLoss: {:.6f}".format(epoch, 100.* batch_i/len(data.X), loss.item()))
                
        model.eval()
        torch.save(model, 'models/gru_model.bin')
        print('Model saved')
        
    print('Training Finished')
