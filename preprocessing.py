import numpy as np
import torch
import re

class Preprocess():
    
    def __init__(self, file_path, seq_length, batch_size):
        self.corpus = self.load_txt(file_path)
        self.corpus = self.clean_corpus(self.corpus)
        self.X, self.y, self.word_list, self.word2id, self.id2word = self.create_training_data(self.corpus, seq_length, batch_size)
        
    
    def load_txt(self, file_path=''):
        print('Loading: ', file_path)
        try:
            with open(file_path, 'r') as f:
                corpus = f.readlines() # save each line as an element in the list
                print('Corpus loaded')
        except FileNotFoundError:
            print('The specified file path not found')

        return corpus
    
    def clean_corpus(self, corpus):
        
        for i, sentence in enumerate(corpus):
            sentence = sentence.replace('\n', '')
            sentence = re.sub(r'\d+:\d+', '', sentence) # remove chapters number of the quote
            corpus[i] = sentence[1:] # remove chapters name

        print('Corpus Cleaned')
        corpus = list(filter(None, corpus))
        return ''.join(corpus)
            
    
    
    def create_training_data(self, corpus, seq_length, batch_size):
        
        print('Creating unique list of characters')
        word_list = sorted(list(set(corpus)))

        # Simple word embedding
        id2word = dict(enumerate(word_list))
        word2id = dict((v, k) for k, v in id2word.items())

        # Create training set
        X = []
        y = []

        for i in range(0, len(corpus)-seq_length):
            seq_X = corpus[i:i+seq_length]
            seq_y = corpus[i+seq_length]

            X.append([word2id[char] for char in seq_X])
            y.append([word2id[seq_y]])

        '''
        train_X, train_y = X[:int(valid_ratio * len(X))], y[:int(valid_ratio * len(y))]
        test_X, test_y = X[int(valid_ratio * len(X)):], y[int(valid_ratio * len(y)):]

        # Make sure the length of set is divisiable by batch size
        train_X = train_X[:len(train_X)-len(train_X)%batch_size]
        train_y = train_y[:len(train_y)-len(train_y)%batch_size]
        test_X = test_X[:len(test_X)-len(test_X)%batch_size]
        test_y = test_y[:len(test_y)-len(test_y)%batch_size]


        # Pytorch LSTM layer take in 3 dim matrix (sequence, batch, element)
        train_X = np.array(train_X)
        train_X = train_X.reshape(-1, batch_size, train_X.shape[1]) # -1 means determined by other two
        train_X = torch.cuda.LongTensor(train_X)

        train_y = np.array(train_y)
        train_y = train_y.reshape(-1, batch_size)
        train_y = torch.cuda.LongTensor(train_y)

        test_X = np.array(test_X)
        test_X = test_X.reshape(-1, batch_size, test_X.shape[1])
        test_X = torch.cuda.LongTensor(test_X)

        test_y = np.array(test_y)
        test_y = test_y.reshape(-1, batch_size)
        test_y = torch.cuda.LongTensor(test_y)

        return train_X, train_y, test_X, test_y, word_list, word2id, id2word
        '''
        
        X = X[:len(X) - len(X)%batch_size]
        y = y[:len(y) - len(y)%batch_size]
        
        X = np.array(X)
        X = X.reshape(-1, batch_size, X.shape[1])
        X = torch.LongTensor(X)
        
        y = np.array(y)
        y = y.reshape(-1, batch_size)
        y = torch.LongTensor(y)
        
        return X, y, word_list, word2id, id2word
            
